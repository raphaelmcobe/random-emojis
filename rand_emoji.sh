#!/usr/bin/env bash

random_emoji_index=$(shuf -i1-33 -n1)
random_emoji=$(head -${random_emoji_index} emojis.txt | tail -1)
echo -e "\U${random_emoji}"
